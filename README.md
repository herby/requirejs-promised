# requirejs-promised
RequireJS plugin to get decorated require that returns a promise

## Use
Instead of usual

```js
require(["require", ...], function (require, ...) {
  // ...
  require(["module/id", "module/id2"],
    function (module1, module2) {...}/*, errback*/
  );
  // ...
});
```

you can use

```js
require(["promised!require", ...], function (require, ...) {
  // ...
  require(["module/id", "module/id2"])
    .then(function (moduleArray) {...})
    // .catch(errback)
    ;
  // ...
});
```

or, in ES2015,

```js
require(["promised!require", ...], function (require, ...) {
  // ...
  require(["module/id", "module/id2"])
    .then(([module1, module2]) => {...})
    // .catch(errback)
    ;
  // ...
});
```

With all the niceties of promises, like later attachment of handler, etc.

The global `Promise` must be present. It's your responsibility
to polyfill it if needed
(`require('es6-promise').polyfill();` is an easy way).

In other words, `promised!require` works just like plain `require`
for sync case `require("module/id")`, but in case of async call,
it returns a Promise which resolves to array of modules if succeeded,
or it rejects with an error if failed.
